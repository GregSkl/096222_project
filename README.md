# 096222_Project

In order to run the code:
We've added a folder to our Drive called "pset_project" which contains a shortcut to [GoogleNews-vectors-negative300.bin.gz](https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?resourcekey=0-wjGZdNAUop6WykTtMip30g) which is the Word2Vec model.
The notebook requires signing in to the Drive account and the inclusion of said structure.

## Open Ended Task: 
Comparing Ridge Performance with Linear Regression 
